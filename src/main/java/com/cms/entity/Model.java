package com.cms.entity;

import java.util.List;

import com.cms.entity.base.BaseModel;
import com.cms.util.DbUtils;
import com.jfinal.plugin.activerecord.Page;

/**
 * Entity - 模型
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class Model extends BaseModel<Model> {
    
    //单页
    public static int PAGE_TYPE=1;
    //列表
    public static int LIST_TYPE=2;
    
    //启用
    public static int NORMAL_STATUS=1;
    //禁用
    public static int UNNORMAL_STATUS=0;
    
	
	/**
	 * 查找模型分页
	 * 
	 * @param pageNumber
	 *            页码
	 * @param pageSize
	 *            每页记录数
	 * @return 模型分页
	 */
	public Page<Model> findPage(Integer pageNumber,Integer pageSize){
		String filterSql = "";
	    String orderBySql = DbUtils.getOrderBySql("createDate desc");
		return paginate(pageNumber, pageSize, "select *", "from cms_model where 1=1 "+filterSql+orderBySql);
	}
	
	/**
	 * 查找启用模型列表
	 * 
	 * @return 启用模型列表
	 */
	public List<Model> findNormalList(){
	    String filterSql = " and status="+NORMAL_STATUS;
	    String orderBySql = DbUtils.getOrderBySql("createDate desc");
		return find("select * from cms_model where 1=1 "+filterSql+orderBySql);
	}
}
